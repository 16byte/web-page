module.exports = function(app)
{
	var express = require('express');
	var router = express.Router();
	var octicons = require("octicons");
	var teams = require('../models/team');
	var boardContents = require('../models/board');

	app.get('/', function(req, res) {

		teams.find({}).exec(function(err, rawContents) {
				if (err)
					throw err;

				res.render('index', {
					title: "Index",
					mail_icon: octicons.mail.toSVG( { "Width": "20%", "height": "20%"}),
					contents: rawContents
				});
		});
	});

	app.get('/junyoung', function(req, res) {
		res.render('junyoung', {
			title: "Introduce",
			mail_icon: octicons.mail.toSVG( { "Width": "20%", "height": "20%"}),
		});
	});

	app.get('/huiyun', function(req, res) {
		res.render('huiyun', {
			title: "Introude"
		});
	})

	app.get('/huiyun2', function(req, res) {
		res.render('huiyun2', {
			title: "History"
		});
	})

	app.get('/dongwok', function(req, res) {
		res.render('dongwok', {
			title: "Introude"
		});
	})

	app.get('/junhwan', function(req, res) {
		res.render('junhwan', {
			title: "Introude"
		});
	})

	app.get('/project', function(req, res) {
		res.render('project', {
			title: "RaspberryPi Project",
		});
	});

	app.get('/board', function(req, res) {
		var page = (typeof(req.query.page) != "undefined") ? ((req.query.page > 0) ? req.query.page : 1) : 1;
		console.log(page);

		boardContents.find({}).sort({post_time:-1}).exec(function (err, rawContents) {
			if (err)
				throw err;

			res.render('board', {
				title: "Board",
				person_icon: octicons.person.toSVG({ "width": 24, "height": 24 }),
				like_icon: octicons.thumbsup.toSVG({ "width": 12, "height": 12 }),
				history_icon: octicons.history.toSVG({ "width": 12, "height": 12 }),
				eye_icon: octicons.eye.toSVG({ "width": 12, "height": 12 }),
				comment_icon: octicons.comment.toSVG({ "width": 12, "height": 12 }),
				pencil_icon: octicons.pencil.toSVG({ "height": 14 }),
				page: page,
				divider: 10,
				contents: rawContents
			});
		});
	});

	app.get('/view', function(req, res) {
		var board_id = req.query.id;
		console.log("Board id = " + board_id);
		if (typeof(board_id) == "undefined") {
			res.redirect('/board');
			return;
		}

		boardContents.findOne({_id: board_id}, function(err, rawContent) {
			if (err) {
				console.log("aefaef")
				throw err;
				return;
			}

			console.log("raw content = " + rawContent)
			if (rawContent === null) {
				res.redirect('/board');
				return;
			}

			rawContent.hits += 1;
			rawContent.save(function(err) {
				if (err)
					throw err;

				console.log(rawContent);
	
				res.render('boardview', {
					title: "Board",
					idx: board_id,
					like_icon: octicons.thumbsup.toSVG({ "width": 15, "height": 15}),
					tools_icon: octicons.tools.toSVG({ "width": 15, "height": 15 }),
					can_icon: octicons.trashcan.toSVG({ "width": 15, "height": 15 }),
					person_icon: octicons.person.toSVG({ "width": 36, "height": 36 }),
					content: rawContent
				});
			});
		})
	});

	app.post('/search', function(req, res) {
		var query_word = req.body.q;

		console.log("Query Word = " + query_word);
		if (typeof(query_word) == "undefined")
			res.end();
	
		var query_cond = {$regex: query_word};

		boardContents.find({$or:[{title: query_cond}, {contents: query_cond}, {writer: query_cond}]}).sort({post_time:-1}).exec(function(err, resultContents) {
			if (err)
				throw err;

			res.render('board', {
				title: "Board",
				query_word: query_word,
				person_icon: octicons.person.toSVG({ "width": 24, "height": 24 }),
				like_icon: octicons.thumbsup.toSVG({ "width": 12, "height": 12 }),
				history_icon: octicons.history.toSVG({ "width": 12, "height": 12 }),
				eye_icon: octicons.eye.toSVG({ "width": 12, "height": 12 }),
				comment_icon: octicons.comment.toSVG({ "width": 12, "height": 12 }),
				pencil_icon: octicons.pencil.toSVG({ "height": 14 }),
				divider: 10,
				page: 1,
				contents: resultContents
			});
		});
	});

	app.post('/board/write', function(req, res) {
		console.log("Received Write API");		

		var Board = new boardContents();

		Board.title = req.body.inputTitle;
		Board.password = req.body.inputPassword;
		Board.contents = req.body.inputContent;
		Board.writer = req.body.inputName;
		Board.description = req.body.inputDesc;

		Board.save(function(err) {
			if (err) {
				console.error(err);
				res.end();
			}

			res.redirect("/board");
		});
	});

	app.post('/board/modify', function(req, res) {
		var inputid = req.body.modify_inputid;
		var pwd = req.body.modify_inputpwd;

		console.log("idx = " + inputid + " pass = " + pwd);

		boardContents.update({ _id: inputid, password: pwd }, { $set: { title: req.body.inputTitle, writer: req.body.inputName, contents: req.body.inputContent, description: req.body.inputDesc, password: req.body.inputPassword } }, function(err, output) {
			if (err)
				throw err;

			console.log(output);
			if (output.n) {
				if (output.nModified) {
					console.log('updated');
					res.redirect("/view?id=" + inputid);
					return;
				}
			}

			res.redirect("/view?id=" + inputid);
		});
	});

	app.post('/board/remove', function(req, res) {
		var idx = req.body.articleId;
		var pass = req.body.articlePwd;

		boardContents.remove({_id: idx, password: pass}, function(err, output) {
			if (err) {
				console.error(err);
				throw err;
			}

			if (output.result.n > 0) {
				res.redirect('/board');
			} else {
				res.redirect('/view?id=' + idx);
			}
		});
	});

	app.post('/comment', function(req, res) {
		var idx = req.body.cidx;
		var name = req.body.cName;
		var pass = req.body.cPwrd;
		var comment = req.body.comment;

		boardContents.findOne({_id: idx}, function(err, content) {
			content.comments.unshift({name: name, password: pass, memo: comment});
			content.save(function(err) {
				if (err)
					throw err;
			});
		});

		res.redirect('/view?id=' + idx);
	}); 

	app.post('/comment/remove', function(req, res) {
		var idx = req.body.cidx;
		var password = req.body.commentPwd;
		var cidx = req.body.commentId;

		boardContents.update({_id: idx}, { $pull : { comments : { _id : cidx, password: password }}}, function(err, output) {
			if (err)
				throw err;
		});

		res.redirect('/view?id=' + idx);
	}); 

	app.get('/contactus', function(req, res) {
		res.render('contactus', {
			title: 'Contact us'
		});
	});

	var nodemailer = require('nodemailer');

	var transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'segment12123@gmail.com',
			pass: ''
		}
	});
	app.post('/contactus', function(req, res) {
		var mailOptions = {
			from: 'segment12123@gmail.com',
			to: req.body.inputEmail,
			subject: '[no-reply] Your request have been recived',
			text: req.body.comment
		};

		transporter.sendMail(mailOptions, function (err, info) {
			if (err) {
				console.log('error: ' + err);
				throw err;
			}

			console.log('Email sent: ' + info.response);
		});

		var mailOptions2 = {
			from: 'segment12123@gmail.com',
			to: 'segment12123@gmail.com',
			subject: "Received some message from " + req.body.inputName,
			text: "Name: " + req.body.inputName + " Phone: " + req.body.inputPhone + " Email: " + req.body.inputEmail + " Comment: " + req.body.comment
		};

		transporter.sendMail(mailOptions2, function (err, info) {
			if (err) {
				console.log('error: ' + err);
				throw err;
			}

			console.log('Email sent: ' + info.response);
			transporter.close();
		});

		res.redirect('/contactus');
	});
		
}
