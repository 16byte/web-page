var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var boardSchema = new Schema({
	title: String,
	description: String,
	password: String,
	contents: String,
	comments:[{
		name: String,
		password: String,
		memo: String,
		date: {type:Date, default: Date.now}
	}],
	writer: String,
	post_time: { type: Date, default: Date.now },
	hits: { type: Number, default: 0 },
	like: { type: Number, default: 0 }
});

module.exports = mongoose.model('boardContents', boardSchema);
