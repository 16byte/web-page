var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var teamSchema = new Schema({
	name: String,
	description: String,
	studentid: String,
	instagram: { type: String, Default: "" },
	facebook: { type: String, Defeault: "" },
	email: { type: String, Default: "" },
	birthday: String,
	phone: String,
	link: String,
	pic_link: { type: String, Default: "" }
});

module.exports = mongoose.model('teams', teamSchema);
