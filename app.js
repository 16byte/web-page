var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var promise = mongoose.connect('mongodb://localhost/webdb', {
	useMongoClient: true
});

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(express.static('public'));

var router = require('./routes')(app)

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	console.log('Connected to mongod server');
});

var port = process.env.PORT || 8081;
var server = app.listen(port, function() {
	console.log('Express server has started on port ' + port);
});
